﻿using System;
using System.Management;

namespace SVNManagementAddIn
{
    class RepositoryHelper
    {
        #region 仓库管理

        /// <summary>
        /// 创建仓库
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool CreateRepository(string name)
        {
            try
            {
                var repository = new ManagementClass(VisualSVN.ROOT, VisualSVN._Repository, null);
                ManagementBaseObject @params = repository.GetMethodParameters("Create"); //创建方法参数引用

                @params["Name"] = name.Trim(); //传入参数

                repository.InvokeMethod("Create", @params, null); //执行
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 创建仓库目录
        /// </summary>
        /// <param name="repositories"> </param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool CreateRepositoryFolders(string repositories, string[] name)
        {
            try
            {
                var repository = new ManagementClass(VisualSVN.ROOT, VisualSVN._Repository, null);
                ManagementObject instance = repository.CreateInstance();
                if (instance != null)
                {
                    instance.SetPropertyValue("Name", repositories);
                    ManagementBaseObject @params = repository.GetMethodParameters("CreateFolders");
                    @params["Folders"] = name;
                    @params["Message"] = "";
                    instance.InvokeMethod("CreateFolders", @params, null);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 重命名仓库
        /// </summary>
        /// <param name="oldname"></param>
        /// <param name="newname"></param>
        /// <returns></returns>
        public static bool RenameRepository(string oldname, string newname)
        {
            try
            {
                var repository = new ManagementClass(VisualSVN.ROOT, VisualSVN._Repository, null);
                ManagementBaseObject @params = repository.GetMethodParameters("Rename"); //创建方法参数引用

                @params["OldName"] = oldname.Trim();//传入参数
                @params["NewName"] = newname.Trim();//传入参数

                repository.InvokeMethod("Rename", @params, null); //执行
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 删除仓库
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool DeleteRepository(string name)
        {
            try
            {
                var repository = new ManagementClass(VisualSVN.ROOT, VisualSVN._Repository, null);
                ManagementBaseObject @params = repository.GetMethodParameters("Delete"); //创建方法参数引用

                @params["Name"] = name.Trim();//传入参数

                repository.InvokeMethod("Delete", @params, null); //执行
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 根据仓库名取得仓库实体
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static ManagementObject GetRepositoryObject(string name)
        {
            return new ManagementObject(VisualSVN.ROOT, string.Format("VisualSVN_Repository.Name='{0}'", name), null);
        }

        #endregion
    }
}
