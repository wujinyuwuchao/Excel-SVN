﻿using System;
using System.Management;

namespace SVNManagementAddIn
{
    class UserHelper
    {
        #region 用户管理

        /// <summary>
        /// 创建用户
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool CreateUser(string username, string password)
        {
            try
            {
                var user = new ManagementClass(VisualSVN.ROOT, VisualSVN._User, null);
                ManagementBaseObject @params = user.GetMethodParameters("Create");

                @params["Name"] = username.Trim();
                @params["Password"] = password.Trim();

                user.InvokeMethod("Create", @params, null);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 设置用户所属的组
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="groupNames"></param>
        /// <param name="operTypes"></param>
        /// <returns></returns>
        public static bool SetMemberGroup(string userName, string groupNames, string operTypes)
        {
            string[] groups = groupNames.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string group in groups)
            {
                GroupHelper.SetGroupMembers(group, userName, operTypes);
            }
            return true;
        }

        /// <summary>
        /// 设置用户密码
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static bool SetPassword(string username, string password)
        {
            try
            {
                var user = new ManagementClass(VisualSVN.ROOT, VisualSVN._User, null);
                ManagementObject instance = user.CreateInstance();
                if (instance != null)
                {
                    instance.SetPropertyValue("Name", username.Trim());
                    ManagementBaseObject @params = instance.GetMethodParameters("SetPassword");

                    @params["Password"] = password.Trim();

                    instance.InvokeMethod("SetPassword", @params, null);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static bool DeleteUser(string username)
        {
            try
            {
                var user = new ManagementClass(VisualSVN.ROOT, VisualSVN._User, null);
                ManagementBaseObject @params = user.GetMethodParameters("Delete");

                @params["Name"] = username.Trim();

                user.InvokeMethod("Delete", @params, null);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
    }
}
